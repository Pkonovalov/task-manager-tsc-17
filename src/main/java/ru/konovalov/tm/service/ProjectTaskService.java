package ru.konovalov.tm.service;

import ru.konovalov.tm.api.repository.IProjectRepository;
import ru.konovalov.tm.api.service.IProjectTaskService;
import ru.konovalov.tm.api.repository.ITaskRepository;
import ru.konovalov.tm.exeption.empty.EmptyIdException;
import ru.konovalov.tm.exeption.empty.EmptyNameException;
import ru.konovalov.tm.exeption.entity.ProjectNotFoundException;
import ru.konovalov.tm.model.Project;
import ru.konovalov.tm.model.Task;

import java.util.List;

import static ru.konovalov.tm.util.ValidationUtil.isEmpty;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService( IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findALLTaskByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        return taskRepository.findALLTaskByProjectId(projectId);
    }

    @Override
    public Task assignTaskByProjectId(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        return taskRepository.assignTaskByProjectId(projectId, taskId);
    }

    @Override
    public Task unassignTaskByProjectId(final String taskId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        return taskRepository.unassignTaskByProjectId(taskId);
    }

    @Override
    public List<Task> removeTasksByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        return taskRepository.removeAllTaskByProjectId(projectId);
    }

    @Override
    public Project removeProjectById(final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        taskRepository.removeAllTaskByProjectId(projectId);
        projectRepository.removeOneById(projectId);
        return null;
    }

    @Override
    public void removeProjectByName(final String projectName) {
        if (isEmpty(projectName)) throw new EmptyNameException();
        String projectId = projectRepository.getIdByName(projectName);
        if (isEmpty(projectId)) throw new EmptyIdException();
        taskRepository.removeAllTaskByProjectId(projectId);
        projectRepository.removeOneByName(projectName);
    }

    @Override
    public void clearTasks() {
        taskRepository.removeAllBinded();
        projectRepository.clear();
    }

}

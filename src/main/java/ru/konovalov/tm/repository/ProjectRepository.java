package ru.konovalov.tm.repository;

import ru.konovalov.tm.api.repository.IProjectRepository;
import ru.konovalov.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public List<Project> findAll(Comparator<Project> comparator) {
        final List<Project> projectList = new ArrayList<>(projects);
        projectList.sort(comparator);
        return projectList;
    }

    @Override
    public void add(final Project project) {
        projects.add(project);
    }

    @Override
    public void remove(final Project project) {
        projects.remove(project);
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public Project findOneById(final String id) {
        for (final Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project removeOneById(final String id) {
        final Project project = findOneById(id);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        return projects.get(index);
    }

    @Override
    public Project removeOneByIndex(final Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        remove(project);
        return project;
    }

    @Override
    public Project findOneByName(final String name) {
        for (final Project project : projects)
            if (name.equals(project.getName())) return project;
        return null;
    }

    @Override
    public Project removeOneByName(final String name) {
        final Project project = findOneByName(name);
        if (project == null) return null;
        remove(project);
        return project;
    }

    public boolean existsByName(String name) {
        for (final Project project : projects) {
            if (name.equals(project.getName())) return true;
        }
        return false;
    }

    public boolean existsById(String id) {
        for (final Project project : projects) {
            if (id.equals(project.getId())) return true;
        }
        return false;
    }

    @Override
    public int size() {
        return projects.size();
    }

    @Override
    public String getIdByName(String name) {
        for (final Project project : projects) {
            if (name.equals(project.getName())) return project.getId();
        }
        return null;
    }

    @Override
    public String getIdByIndex(Integer index) {
        return projects.get(index).getId();
    }



}



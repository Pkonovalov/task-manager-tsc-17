package ru.konovalov.tm.repository;

import ru.konovalov.tm.api.repository.ITaskRepository;
import ru.konovalov.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static ru.konovalov.tm.util.ValidationUtil.isEmpty;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public List<Task> findALLTaskByProjectId(final String projectId) {
        List<Task> taskList = new ArrayList<>();
        for (Task task : tasks) {
            if (projectId.equals(task.getProjectId())) taskList.add(task);
        }
        return taskList;
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public List<Task> findAll(Comparator<Task> comparator) {
        final List<Task> taskList = new ArrayList<>(tasks);
        taskList.sort(comparator);
        return taskList;
    }

    @Override
    public List<Task> removeAllTaskByProjectId(final String projectId) {
        List<Task> tasksList = new ArrayList<>();
        for (Task task : tasks) {
            if (projectId.equals(task.getProjectId())) tasksList.add(task);
        }
        return tasksList;
    }

    @Override
    public Task assignTaskByProjectId(final String projectId, final String taskId) {
        final Task task = findOneById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unassignTaskByProjectId(final String taskId) {
        final Task task = findOneById(taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    @Override
    public void add(Task task) {
        tasks.add(task);
    }

    @Override
    public void remove(final Task task) {
        tasks.remove(task);
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public Task findOneById(final String id) {
        for (final Task task : tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneById(final String id) {
        final Task task = findOneById(id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        return tasks.get(index);
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Task findOneByName(final String name) {
        for (final Task task : tasks)
            if (name.equals(task.getName())) return task;
        return null;
    }

    @Override
    public Task removeOneByName(final String name) {
        final Task task = findOneByName(name);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public void removeAllBinded() {
        for (int i = tasks.size(); i-- > 0; ) {
            if (!isEmpty(tasks.get(i).getProjectId())) {
                tasks.remove(i);
            }
        }
    }

    @Override
    public int size() {
        return tasks.size();
    }

    @Override
    public boolean existsById(String id) {
        for (final Task task : tasks) {
            if (id.equals(task.getId())) return true;
        }
        return false;
    }


    @Override
    public boolean existsByName(final String name) {
        for (final Task task : tasks) {
            if (name.equals(task.getName())) return true;
        }
        return false;
    }

    @Override
    public String getIdByIndex(int index) {
        return tasks.get(index).getId();
    }

}









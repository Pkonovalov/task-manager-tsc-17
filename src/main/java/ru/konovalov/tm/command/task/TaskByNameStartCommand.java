package ru.konovalov.tm.command.task;

import ru.konovalov.tm.util.TerminalUtil;

public class TaskByNameStartCommand extends AbstractTaskCommand{
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-start-by-name";
    }

    @Override
    public String description() {
        return "Start task by name";
    }

    @Override
    public void execute() {
        System.out.println("[START TASK]");
        System.out.println("ENTER NAME:");
        serviceLocator.getTaskService().startTaskByName(TerminalUtil.nextLine());
    }

}

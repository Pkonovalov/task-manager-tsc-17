package ru.konovalov.tm.command.task;

import ru.konovalov.tm.util.TerminalUtil;

public class TaskByIdFinishCommand extends AbstractTaskCommand{

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-finish-by-id";
    }

    @Override
    public String description() {
        return "Finish task by id";
    }

    @Override
    public void execute() {
        System.out.println("[START TASK]");
        System.out.println("ENTER ID:");
        serviceLocator.getTaskService().finishTaskById(TerminalUtil.nextLine());
    }

}

package ru.konovalov.tm.command.task;

import ru.konovalov.tm.exeption.entity.TaskNotFoundException;
import ru.konovalov.tm.util.TerminalUtil;

public final class TaskByProjectIdBindCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-bind";
    }

    @Override
    public String description() {
        return "Bind task by project id";
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT BY ID]");
        if (serviceLocator.getTaskService().size() < 1) throw new TaskNotFoundException();
        System.out.println("ENTER TASK INDEX:");
        final String taskId = serviceLocator.getTaskService().getIdByIndex(TerminalUtil.nextNumber() - 1);
        System.out.println("ENTER PROJECT ID:");
        serviceLocator.getProjectTaskService().assignTaskByProjectId(TerminalUtil.nextLine(), taskId);
    }

}

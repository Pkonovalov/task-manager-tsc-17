package ru.konovalov.tm.command.task;

import ru.konovalov.tm.enumerated.Status;
import ru.konovalov.tm.exeption.entity.TaskNotFoundException;
import ru.konovalov.tm.util.TerminalUtil;

import java.util.Arrays;

public class TaskByNameSetStatusCommand extends AbstractTaskCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-set-status-by-name";
    }

    @Override
    public String description() {
        return "Set task status by name";
    }

    @Override
    public void execute() {
        System.out.println("[SET TASK STATUS]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        if (!serviceLocator.getTaskService().existsByName(name)) throw new TaskNotFoundException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        serviceLocator.getTaskService().changeTaskStatusByName(name, Status.getStatus(TerminalUtil.nextLine()));
    }

}

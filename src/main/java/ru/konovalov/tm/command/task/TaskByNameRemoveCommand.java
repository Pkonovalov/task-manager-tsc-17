package ru.konovalov.tm.command.task;

import ru.konovalov.tm.util.TerminalUtil;

public final class TaskByNameRemoveCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-remove-by-name";
    }

    @Override
    public String description() {
        return "Remove task by name";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER NAME:");
        serviceLocator.getTaskService().removeOneByName(TerminalUtil.nextLine());
    }

}

package ru.konovalov.tm.command.task;

import ru.konovalov.tm.enumerated.Status;
import ru.konovalov.tm.exeption.entity.TaskNotFoundException;
import ru.konovalov.tm.model.Task;
import ru.konovalov.tm.util.TerminalUtil;

import java.util.Arrays;

public class TaskByIdSetStatusCommand extends AbstractTaskCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-set-status-by-id";
    }

    @Override
    public String description() {
        return "Set task status by id";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = serviceLocator.getTaskService().changeTaskStatusById(id, status);
        if (task == null) throw new TaskNotFoundException();
        else System.out.println("[OK]");
    }

}

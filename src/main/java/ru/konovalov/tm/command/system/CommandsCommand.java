package ru.konovalov.tm.command.system;

import ru.konovalov.tm.command.AbstractCommand;
import ru.konovalov.tm.model.Command;

import java.util.Collection;

public final class CommandsCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-c";
    }

    @Override
    public String name() {
        return "commands";
    }

    @Override
    public String description() {
        return "Show all commands";
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command : commands) System.out.println(command.name());
    }

}

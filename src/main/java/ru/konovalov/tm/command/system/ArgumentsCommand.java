package ru.konovalov.tm.command.system;

import ru.konovalov.tm.command.AbstractCommand;
import ru.konovalov.tm.model.Command;

import java.util.Collection;

import static ru.konovalov.tm.util.ValidationUtil.isEmpty;

public final class ArgumentsCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String name() {
        return "arguments";
    }

    @Override
    public String description() {
        return "Show all arguments";
    }

    @Override
    public void execute() {
    System.out.println("[ARGUMENTS]");
        final Collection<AbstractCommand> arguments = serviceLocator.getCommandService().getArguments();
        for (final AbstractCommand argument : arguments) {
            final String arg = argument.arg();
            if (isEmpty(arg)) continue;
            System.out.println(arg);
        }
    }
}

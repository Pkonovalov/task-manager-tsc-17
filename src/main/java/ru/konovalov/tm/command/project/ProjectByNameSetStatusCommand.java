package ru.konovalov.tm.command.project;

import ru.konovalov.tm.enumerated.Status;
import ru.konovalov.tm.exeption.entity.ProjectNotFoundException;
import ru.konovalov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectByNameSetStatusCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-set-status-by-name";
    }

    @Override
    public String description() {
        return "Set project status by name";
    }

    @Override
    public void execute() {
        System.out.println("[SET PROJECT STATUS]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        if (!serviceLocator.getProjectService().existsByName(name)) throw new ProjectNotFoundException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        serviceLocator.getProjectService().changeProjectStatusByName(name, Status.getStatus(TerminalUtil.nextLine()));
    }

}

package ru.konovalov.tm.command.project;

import ru.konovalov.tm.util.TerminalUtil;

public final class ProjectByIdFinishCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-finish-by-id";
    }

    @Override
    public String description() {
        return "Finish project by id";
    }

    @Override
    public void execute() {
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER ID:");
        serviceLocator.getProjectService().finishProjectById(TerminalUtil.nextLine());
    }

}

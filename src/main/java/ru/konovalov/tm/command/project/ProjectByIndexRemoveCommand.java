package ru.konovalov.tm.command.project;

import ru.konovalov.tm.util.TerminalUtil;

public class ProjectByIndexRemoveCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-remove-by-index";
    }

    @Override
    public String description() {
        return "Remove project by index";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER INDEX:");
        serviceLocator.getProjectService().removeProjectByIndex(TerminalUtil.nextNumber() - 1);
    }

}

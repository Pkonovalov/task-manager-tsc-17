package ru.konovalov.tm.command.project;

import ru.konovalov.tm.util.TerminalUtil;

public class ProjectByIdRemoveCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-remove-by-id";
    }

    @Override
    public String description() {
        return "Remove project by id";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER ID:");
        serviceLocator.getProjectTaskService().removeProjectById(TerminalUtil.nextLine());
    }

}

package ru.konovalov.tm.command.project;

import ru.konovalov.tm.enumerated.Status;
import ru.konovalov.tm.exeption.system.IndexIncorrectException;
import ru.konovalov.tm.util.TerminalUtil;

import java.util.Arrays;

import static ru.konovalov.tm.util.ValidationUtil.checkIndex;

public class ProjectByIndexSetStatusCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-set-status-by-index";
    }

    @Override
    public String description() {
        return "Set project status by index";
    }

    @Override
    public void execute() {
        System.out.println("[SET PROJECT STATUS]");
        System.out.println("ENTER INDEX:");
        final int index = TerminalUtil.nextNumber() - 1;
        if (!checkIndex(index, serviceLocator.getProjectService().size())) throw new IndexIncorrectException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        serviceLocator.getProjectService().changeProjectStatusByIndex(index, Status.getStatus(TerminalUtil.nextLine()));
    }


}

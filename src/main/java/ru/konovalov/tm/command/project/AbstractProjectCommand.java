package ru.konovalov.tm.command.project;

import ru.konovalov.tm.command.AbstractCommand;
import ru.konovalov.tm.exeption.entity.ProjectNotFoundException;
import ru.konovalov.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProject(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + project.getStatus().getDisplayName());
        System.out.println("CREATED: " + project.getCreated());
        System.out.println("STARTED: " + project.getDateStart());
        System.out.println("FINISHED: " + project.getDateFinish());
    }

}

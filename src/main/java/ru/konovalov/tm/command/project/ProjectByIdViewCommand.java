package ru.konovalov.tm.command.project;


import ru.konovalov.tm.util.TerminalUtil;

public class ProjectByIdViewCommand extends AbstractProjectCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return null;
    }

    @Override
    public String description() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        showProject(serviceLocator.getProjectService().findOneById(TerminalUtil.nextLine()));
}

}

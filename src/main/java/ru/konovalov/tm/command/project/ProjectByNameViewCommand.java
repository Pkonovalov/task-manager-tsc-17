package ru.konovalov.tm.command.project;

import ru.konovalov.tm.util.TerminalUtil;

public final class ProjectByNameViewCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-view-by-name";
    }

    @Override
    public String description() {
        return "View project by name";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        showProject(serviceLocator.getProjectService().findOneByName(TerminalUtil.nextLine()));
    }

}

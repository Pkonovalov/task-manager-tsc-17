package ru.konovalov.tm.command.project;


import ru.konovalov.tm.util.TerminalUtil;

public class ProjectByIdStartCommand extends AbstractProjectCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-start-by-id";
    }

    @Override
    public String description() {
        return "Start project by id";
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER ID:");
        serviceLocator.getProjectService().startProjectById(TerminalUtil.nextLine());
    }

}


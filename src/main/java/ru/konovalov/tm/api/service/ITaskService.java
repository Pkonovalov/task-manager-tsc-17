package ru.konovalov.tm.api.service;

import ru.konovalov.tm.enumerated.Status;
import ru.konovalov.tm.model.Project;
import ru.konovalov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    Task add(String name, String description);

    void add(Task task);

    void remove(Task task);

    void clear();

    Task removeOneById(String id);

    Task findOneById(String id);

    Task findOneByName(String name);

    Task removeOneByName(String name);

    Task removeTaskByIndex(Integer index);

    Task findOneByIndex(Integer index);

    Task updateTaskByIndex(Integer index, String name, String description);

    Task updateTaskByName(String name, String nameNew, String description);

    Task updateTaskById(String id, String name, String description);

    Task finishTaskById(String id);

    Task finishTaskByName(String name);

    Task startTaskById(String id);

    Task startTaskByIndex(Integer index);

    Task startTaskByName(String name);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByName(String name, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

    String getIdByIndex(Integer index);

    int size();

    boolean existsById(String id);

    boolean existsByName(String name);
}

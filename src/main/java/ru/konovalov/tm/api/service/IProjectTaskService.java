package ru.konovalov.tm.api.service;

import ru.konovalov.tm.model.Project;
import ru.konovalov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findALLTaskByProjectId(String projectId);

    Task assignTaskByProjectId(String projectId, String taskId);

    Task unassignTaskByProjectId(String taskId);

    List<Task> removeTasksByProjectId(String projectId);

    Project removeProjectById(String projectId);

    void removeProjectByName(String projectName);

    void clearTasks();
}

package ru.konovalov.tm.api.repository;

import ru.konovalov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {
    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    void add(Project project);

    void remove(Project project);

    void clear();

    Project findOneById(String id);

    Project removeOneById(String id);

    Project findOneByIndex(Integer index);

    Project removeOneByIndex(Integer index);

    Project findOneByName(String name);

    Project removeOneByName(String name);

    boolean existsByName(String name);

    boolean existsById(String id);

    int size();

    String getIdByName(String name);

    String getIdByIndex(Integer index);
}

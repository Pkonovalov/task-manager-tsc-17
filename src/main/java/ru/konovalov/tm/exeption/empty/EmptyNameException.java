package ru.konovalov.tm.exeption.empty;


import ru.konovalov.tm.exeption.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error. Name is empty");
    }

}
